(function($, win, doc, undefined){

    var $html = $('html'),
        $body = $('body'),
        $window = $(window),
        $document  = $(document),
        getUrl = window.location;
        basePath = getUrl .protocol + "//" + getUrl.host + "/",

        is_xs = function(){ return $window.width() < 769; },

        jasonurl = "data/chapters.json";
        jsonIdurl = "data/";
        worldChaptersNumber = 0;
        continentChaptersNumber = 0;
        countryChaptersNumber = 0;
        jsonData = "";
        chapterData = "";
        jsonIdData = "";
        var obj = ""
        var continentBack = "";
        var clicked = 0;
        //var chaptersId = "{&quot;chapterIds&quot;:[{&quot;idBackEnd&quot;:508,&quot;idFrontEnd&quot;:&quot;Bergamo&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:509,&quot;idFrontEnd&quot;:&quot;Napoli&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:510,&quot;idFrontEnd&quot;:&quot;Brescia&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:511,&quot;idFrontEnd&quot;:&quot;Padova-Rovigo&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:512,&quot;idFrontEnd&quot;:&quot;Verona&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:513,&quot;idFrontEnd&quot;:&quot;Varese&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:514,&quot;idFrontEnd&quot;:&quot;Roma&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:515,&quot;idFrontEnd&quot;:&quot;Ferrara&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:516,&quot;idFrontEnd&quot;:&quot;Torino&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:517,&quot;idFrontEnd&quot;:&quot;Lecco-Sondrio&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:518,&quot;idFrontEnd&quot;:&quot;Catania&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:519,&quot;idFrontEnd&quot;:&quot;Monza&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:520,&quot;idFrontEnd&quot;:&quot;Rimini&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:521,&quot;idFrontEnd&quot;:&quot;Pavia&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:522,&quot;idFrontEnd&quot;:&quot;Firenze&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:523,&quot;idFrontEnd&quot;:&quot;Lecce-Brindisi&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:524,&quot;idFrontEnd&quot;:&quot;Palermo&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:525,&quot;idFrontEnd&quot;:&quot;Milano&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:526,&quot;idFrontEnd&quot;:&quot;Pescara-Chieti&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:527,&quot;idFrontEnd&quot;:&quot;Bologna&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:528,&quot;idFrontEnd&quot;:&quot;Ancona&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:529,&quot;idFrontEnd&quot;:&quot;Como&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:530,&quot;idFrontEnd&quot;:&quot;Sardegna&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:531,&quot;idFrontEnd&quot;:&quot;Genova&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:532,&quot;idFrontEnd&quot;:&quot;Bari&quot;, &quot;location&quot;:&quot;italia&quot;},{&quot;idBackEnd&quot;:533,&quot;idFrontEnd&quot;:&quot;Barcellona&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:534,&quot;idFrontEnd&quot;:&quot;Dublino&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:535,&quot;idFrontEnd&quot;:&quot;Ginevra&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:536,&quot;idFrontEnd&quot;:&quot;Vienna&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:537,&quot;idFrontEnd&quot;:&quot;Lisbona&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:538,&quot;idFrontEnd&quot;:&quot;Bruxelles&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:539,&quot;idFrontEnd&quot;:&quot;Mosca&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:540,&quot;idFrontEnd&quot;:&quot;Francoforte&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:541,&quot;idFrontEnd&quot;:&quot;Amsterdam&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:542,&quot;idFrontEnd&quot;:&quot;Parigi&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:543,&quot;idFrontEnd&quot;:&quot;Londra&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:544,&quot;idFrontEnd&quot;:&quot;Copenhagen&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:545,&quot;idFrontEnd&quot;:&quot;Belgrado&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:546,&quot;idFrontEnd&quot;:&quot;Monaco di Baviera&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:547,&quot;idFrontEnd&quot;:&quot;Principato di Monaco&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:548,&quot;idFrontEnd&quot;:&quot;Berlino&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:549,&quot;idFrontEnd&quot;:&quot;Madrid&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:550,&quot;idFrontEnd&quot;:&quot;Atene&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:551,&quot;idFrontEnd&quot;:&quot;Stoccolma&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:552,&quot;idFrontEnd&quot;:&quot;Lussemburgo&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:553,&quot;idFrontEnd&quot;:&quot;Varsavia&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:554,&quot;idFrontEnd&quot;:&quot;Lugano&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:555,&quot;idFrontEnd&quot;:&quot;Zurigo&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:556,&quot;idFrontEnd&quot;:&quot;Budapest&quot;, &quot;location&quot;:&quot;europa&quot;},{&quot;idBackEnd&quot;:557,&quot;idFrontEnd&quot;:&quot;Los Angeles&quot;, &quot;location&quot;:&quot;america&quot;},{&quot;idBackEnd&quot;:558,&quot;idFrontEnd&quot;:&quot;Toronto&quot;, &quot;location&quot;:&quot;america&quot;},{&quot;idBackEnd&quot;:559,&quot;idFrontEnd&quot;:&quot;Dallas&quot;, &quot;location&quot;:&quot;america&quot;},{&quot;idBackEnd&quot;:560,&quot;idFrontEnd&quot;:&quot;Boston&quot;, &quot;location&quot;:&quot;america&quot;},{&quot;idBackEnd&quot;:561,&quot;idFrontEnd&quot;:&quot;Chicago&quot;, &quot;location&quot;:&quot;america&quot;},{&quot;idBackEnd&quot;:562,&quot;idFrontEnd&quot;:&quot;San Francisco&quot;, &quot;location&quot;:&quot;america&quot;},{&quot;idBackEnd&quot;:563,&quot;idFrontEnd&quot;:&quot;Miami&quot;, &quot;location&quot;:&quot;america&quot;},{&quot;idBackEnd&quot;:564,&quot;idFrontEnd&quot;:&quot;San Paolo&quot;, &quot;location&quot;:&quot;america&quot;},{&quot;idBackEnd&quot;:565,&quot;idFrontEnd&quot;:&quot;New York&quot;, &quot;location&quot;:&quot;america&quot;},{&quot;idBackEnd&quot;:566,&quot;idFrontEnd&quot;:&quot;Washington D.C.&quot;, &quot;location&quot;:&quot;america&quot;},{&quot;idBackEnd&quot;:567,&quot;idFrontEnd&quot;:&quot;Seattle&quot;, &quot;location&quot;:&quot;america&quot;},{&quot;idBackEnd&quot;:568,&quot;idFrontEnd&quot;:&quot;Mumbai&quot;, &quot;location&quot;:&quot;asia_oceania&quot;},{&quot;idBackEnd&quot;:569,&quot;idFrontEnd&quot;:&quot;Hanoi&quot;, &quot;location&quot;:&quot;asia_oceania&quot;},{&quot;idBackEnd&quot;:570,&quot;idFrontEnd&quot;:&quot;Sydney&quot;, &quot;location&quot;:&quot;asia_oceania&quot;},{&quot;idBackEnd&quot;:571,&quot;idFrontEnd&quot;:&quot;Tokyo&quot;, &quot;location&quot;:&quot;asia_oceania&quot;},{&quot;idBackEnd&quot;:572,&quot;idFrontEnd&quot;:&quot;Singapore&quot;, &quot;location&quot;:&quot;asia_oceania&quot;},{&quot;idBackEnd&quot;:573,&quot;idFrontEnd&quot;:&quot;Hong Kong&quot;, &quot;location&quot;:&quot;asia_oceania&quot;},{&quot;idBackEnd&quot;:574,&quot;idFrontEnd&quot;:&quot;Penisola Arabica&quot;, &quot;location&quot;:&quot;asia_oceania&quot;},{&quot;idBackEnd&quot;:575,&quot;idFrontEnd&quot;:&quot;Kuala Lumpur&quot;, &quot;location&quot;:&quot;asia_oceania&quot;},{&quot;idBackEnd&quot;:576,&quot;idFrontEnd&quot;:&quot;Shanghai&quot;, &quot;location&quot;:&quot;asia_oceania&quot;},{&quot;idBackEnd&quot;:577,&quot;idFrontEnd&quot;:&quot;Beijing&quot;, &quot;location&quot;:&quot;asia_oceania&quot;}]}";


    readJsonId = function() {
        if (jsonIdurl == "data/") {

            if (intlLang == "it") {
                jsonIdurl = jsonIdurl + "ChapterTranslations_it_IT.json";
            }
            else if (intlLang == "en") {
                jsonIdurl = jsonIdurl + "ChapterTranslations_en_US.json";
            }
            $.getJSON(jsonIdurl, function (dataC) {
                jsonIdData = dataC;
                console.log(jsonIdData);
            });

        }
    }

    setLoaded = function(refer) {
        $(refer).find(".loadedsvg").addClass("loaded");
    },


    loadWorld = function() {
        $('.map-layer--world .loadedsvg').removeClass("loaded");
        continentBack = "";
        obj = JSON.parse(chaptersId.replace(/&quot;/g,'"'));
        console.log(obj);
        if(is_xs()) {
            var imageUrl = "img/chapters-modified/world-sm-img.svg";
        }
        else {
            var imageUrl = "img/chapters-modified/world-img.svg";
        }
        $('.map-layer--world .loadedsvg').load(imageUrl);
        setTimeout(function() {
            setLoaded(".map-layer--world");
        }, 1000);
        fixIe();
        worldChaptersNumber = obj.chapterIds.length;
        //setNavigation(worldChaptersNumber,"Worldwide");
        setNavigation(69,"Worldwide");
        setTimeout(function() {
            readJsonId();
        }, 2000);
    },

    setNavigation = function(val, label) {
        $(".navigation .nr-ch").html(val);
        console.log(label);
        if(label == "principatomonaco") {
            label = "Principato di Monaco"
        }
        if(label == "Hongkong") {
            label = "Hong Kong"
        }
        if(label == "indipendente") {
            label = "Principato di Monaco";
        }
        console.log(label);
        $(".navigation .nm-ch").html(label);
    },

    fixIe = function() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
        {
            var hWrapper = $(".map-wrap").height();
            for (var i = 0; i < 5; i++) {
                setTimeout(function() {
                    $(".map-layer .loadedsvg svg").css('height',hWrapper);
                    //$(".map-layer .loadedsvg svg").css('height',"auto");
                    console.log("Test IE fix");
                }, 500 * i);
            }
        }
    }


    goToContinent = function (id) {
        $('.map-layer--continent .loadedsvg').removeClass("loaded");
        continentChaptersNumber = 0;
        var mapName = id;
        console.log("mapName = "+mapName);
        console.log("continentBack = "+continentBack);
        if (continentBack == "") {
            var chapterContinentName = mapName;
        } else {
            var chapterContinentName = continentBack;
        }

        if (mapName == "sudamerica" || mapName == "nordamerica" ) {
            $(".map-layer--continent").attr("v-cont",mapName);
            mapName = "america"
        }
        else if (mapName == "asia-oceania" || mapName == "australia" ) {
            $(".map-layer--continent").attr("v-cont",mapName);
            mapName = "asia-oceania"
        }
        else {
            $(".map-layer--continent").attr("v-cont","");
        }


        var imageUrl = "img/chapters-modified/continents/"+ mapName +".svg";

        $('.map-layer--continent .loadedsvg').load(imageUrl);
        setTimeout(function() {
            setLoaded(".map-layer--continent");
        }, 1000);
        fixIe();
        $(".backToContinent").attr("id",chapterContinentName);
        $('.map-layer--continent').attr("c-name",mapName);
        $(".map-layer--world").toggleClass("hidden");
        $(".map-layer--continent").toggleClass("hidden");
        $(".map-layer--continent").addClass("fadeIn animated");
        $(".viewallmap").addClass("hide");
        $(".backmap.backToWorld").toggleClass("hide");
        $('.map-layer--world .loadedsvg').empty();

        if (mapName == "asia-oceania") {
            mapName = "asia_oceania";
        }
        var i;
        if (chapterContinentName == "nordamerica") {
            chapterContinentName = "nord america"
            if (intlLang == "it") {
                setNavigation(10,"nord america");
                continentBack = "nordamerica";
            }
            else if (intlLang == "en") {
                setNavigation(10,"north america");
            }
        }
        else if (chapterContinentName == "sudamerica") {
            continentBack = "sudamerica";
            chapterContinentName = "sud america"
            if (intlLang == "it") {
                setNavigation(2,"sud america");
            }
            else if (intlLang == "en") {
                setNavigation(2,"south america");
            }
        }
        else if (chapterContinentName == "asia-oceania") {
            continentBack = "asia-oceania";
            chapterContinentName = "oceania"
            setNavigation(9,"asia");
        }
        else if (chapterContinentName == "australia") {
            continentBack = "australia";
            chapterContinentName = "australia"
            setNavigation(1,"australia");
        }


        else {
            continentBack = "europa";
            for (i = 0; i < obj.chapterIds.length; ++i) {
                if (obj.chapterIds[i].location == chapterContinentName ) {
                    continentChaptersNumber++;
                    if (obj.chapterIds[i].location == "europa" ) {
                        continentChaptersNumber++;
                        if (obj.chapterIds[i].location == "italia" ) {
                            continentChaptersNumber++;
                        }
                    }
                }
            }
            //setNavigation(continentChaptersNumber,chapterContinentName);
            setNavigation(47,chapterContinentName);
        }

    },

    clickContinent = function () {
        $("body").delegate(".map-layer--world .gotocontinent", "click", function() {
            var mapName = $(this).attr("id");
            goToContinent(mapName);
        }
        )
    },

    goToCountry = function () {
        $('.map-layer--country .loadedsvg').removeClass("loaded");
        $("body").delegate(".map-layer--continent .gotocountry", "click", function(){
            countryChaptersNumber = 0;
            var mapName = $(this).attr("id");
            if (mapName == "australia-2") {
                mapName = "australia";
            }
            else if (mapName == "serbia-2") {
                mapName = "serbia";
            }
            else if (mapName == "new_york-2") {
                mapName = "new york";
            }
            else if (mapName == "serbia-2") {
                mapName = "serbia";
            }
            console.log(mapName);

            var imageUrl = "img/chapters-modified/countries/"+ mapName +".svg";

            $('.map-layer--country .loadedsvg').load(imageUrl);
            setTimeout(function() {
                setLoaded(".map-layer--country");
            }, 1000);
            fixIe();
            $('.map-layer--country').attr("c-name",mapName);
            $(".map-layer--continent").toggleClass("hidden");
            $(".map-layer--country").toggleClass("hidden");
            $(".map-layer--country").addClass("fadeIn animated");
            $(".backmap.backToWorld").toggleClass("hide");
            $(".backmap.backToContinent").toggleClass("hide");
            $('.map-layer--continent .loadedsvg').empty();

            if (mapName == "italia") {
                var i;
                for (i = 0; i < obj.chapterIds.length; ++i) {
                    if (obj.chapterIds[i].location == mapName ) {
                        countryChaptersNumber++;
                    }
                }
                countryChaptersNumber = 22; // sovracrittura schiantata
            }
            else {
                var i;
                //console.log(mapName);
                if(mapName == "principatomonaco") {
                    mapName = "indipendente";
                }
                else if(mapName == "hongkong") {
                    mapName = "hong kong";
                }
                else if(mapName == "australia-2") {
                    mapName = "australia";
                }
                else if (mapName == "newyork") {
                    mapName = "new york";
                }
                else if (mapName == "washingtonDC") {
                    //mapName = "washington d.c.";
                    countryChaptersNumber = 1;
                }
                else if(mapName == "penisola_arabica") {
                    mapName = "penisola arabica";
                }
                //console.log(mapName);
                for (i = 0; i < obj.chapterIds.length; ++i) {
                    if (obj.chapterIds[i].country == mapName ) {
                        countryChaptersNumber++;
                    }
                }
            }
            setNavigation(countryChaptersNumber,mapName);
        }
        )
    };

    getModalDetail = function(idChapter) {

    }

    openModal = function (mId, cX, cY) {
        popoverClean(".map-layer--country .popover");
        var i;
        var idBackend;
        var mIdUpperCase = mId.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });
        if (mIdUpperCase == "Pescara") {
            mIdUpperCase = "Pescara-Chieti";
        }
        else if (mIdUpperCase == "Lecce") {
            mIdUpperCase = "Lecce-Brindisi";
        }
        else if (mIdUpperCase == "Lecco") {
            mIdUpperCase = "Lecco-Sondrio";
        }
        else if (mIdUpperCase == "Principato_di_monaco") {
            mIdUpperCase = "Principato di Monaco";
        }
        else if (mIdUpperCase == "Kuala_lampur") {
            mIdUpperCase = "kuala Lampur";
        }
        else if (mIdUpperCase == "Sao_paolo") {
            mIdUpperCase = "San Paolo";
        }
        else if (mIdUpperCase == "Sanfrancisco") {
            mIdUpperCase = "San Francisco";
        }
        else if (mIdUpperCase == "Los_angeles") {
            mIdUpperCase = "Los Angeles";
        }
        else if (mIdUpperCase == "New_york-2") {
            mIdUpperCase = "New York";
        }
        else if (mIdUpperCase == "Singapore-2") {
            mIdUpperCase = "Singapore";
        }
        else if (mIdUpperCase == "Monaco_di_baviera") {
            mIdUpperCase = "Monaco di Baviera";
        }
        else if (mIdUpperCase == "Lussemburgo-2") {
            mIdUpperCase = "Lussemburgo";
        }
        else if (mIdUpperCase == "Sidney") {
            mIdUpperCase = "Sydney";
        }
        else if (mIdUpperCase == "Hongkong") {
            mIdUpperCase = "Hong Kong";
        }
        else if (mIdUpperCase == "Copenaghen") {
            mIdUpperCase = "Copenhagen";
        }
        else if (mIdUpperCase == "Tokio") {
            mIdUpperCase = "Tokyo";
        }
        else if (mIdUpperCase == "Shangai") {
            mIdUpperCase = "Shanghai";
        }
        else if (mIdUpperCase == "Washington") {
            mIdUpperCase = "Washington D.C.";
        }
        //console.log("mIdUpperCase = "+mIdUpperCase);
        for (i = 0; i < obj.chapterIds.length; ++i) {
            if (obj.chapterIds[i].idFrontEnd == mIdUpperCase ) {
                idBackend = obj.chapterIds[i].idBackEnd;
            }
        }
        var mapId = mId;
        var htmlModal = "";
        var url = basePath + "api/bac-chapter-map.json";
        var modalClose = '<a class="modal-close" href="javascript:;"><span class="icon icon-close"></span></a>'
        var positionLeft = cX;
        var positionTop = cY;
        var parentOffset = $(".map-layer--country").offset();
        var relX = positionLeft - parentOffset.left -5;
        var relY = positionTop - parentOffset.top - 90;
        var chapterleader = "--";
        var chaptermail = '--';
        var permaLink = "";

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            data: { id: idBackend },
            success: function (result) {
                console.log(result);
                if (result.hasOwnProperty("relativeThumbmaticUrl")) {
                    var imgUrl = basePath+result.relativeThumbmaticUrl;
                } else {
                    var imgUrl = '';
                }
                if (result.hasOwnProperty("chapterLeader")) {
                    chapterleader = result.chapterLeader;
                }
                if (result.hasOwnProperty("chapterMail")) {
                    chaptermail = result.chapterMail;
                }
                if ( result.hasOwnProperty("permalink_it_IT") || result.hasOwnProperty("permalink_en_US") ) {
                    if (intlLang == "it") {
                        permaLink = result.permalink_it_IT;
                    } else {
                        permaLink = result.permalink_en_US;
                    }
                }

                var descr = "";
                if (intlLang == "it") {
                    descr = result.description_it_IT;
                } else {
                    descr = result.description_en_US;
                }

                var cName = "";
                if (intlLang == "it") {
                    cName = result.name_it_IT;
                } else {
                    cName = result.name_en_US;
                }

                var moreBtn = "";
                if (intlLang == "it") {
                    moreBtn = "vedi di più";
                } else {
                    moreBtn = "more";
                }

                htmlModal = '<div class="clicklayer"></div><div class="map-modal card-list"><a class="modal-close" href="javascript:;"><span class="icon icon-close"></span></a><div class="m-card"><div class="image-wrap" style="background-image: url('+ imgUrl +')"></div><div class="event-content-wrap"><div class="green smalltitle text-uppercase mb-1">chapter '+ cName +'</div><div class="gray midtext pb-05">Chapter Leader:<b>'+chapterleader+'</b></div><div class="gray midtext mb-1">Email:<b>'+ chaptermail +'</b></div><div class="gray midtext chapter-description">'+ descr +'</div></div><a href="'+ basePath +'permalink/'+ permaLink +'" class="btn btn--transp btn--small">'+ moreBtn +'</a></div></div>';
                $(".map-layer--country .modale").append(htmlModal);
                //$(".map-layer--country .modale").append(modalClose);
                $(".map-layer--country .modale").css("left",relX+"px");
                $(".map-layer--country .modale").css("top",relY+"px");
                if ( relX > 240 ) {
                    $(".map-layer--country .modale").addClass("tooright");
                } else {

                }
                if ( relY > 150 ) {
                    $(".map-layer--country .modale").addClass("toobottom");
                } else {

                }
            },
            error: (function(jqXHR, textStatus, errorThrown) {
                if (textStatus == 'timeout')
                    console.log('The server is not responding');
                if (textStatus == 'error')
                    console.log(errorThrown);
            })
        });


        //console.log(mapId);

    },

    opendetail = function () {
        $("body").delegate(".map-layer--country .loadedsvg #citta g", "click", function(e){
            var cX = e.pageX;
            var cY = e.pageY;
            var mapId = $(this).attr("id");
            openModal(mapId, cX, cY)
            }
        )
    },

    opendetailFromMap = function () {
        $("body").delegate(".map-layer--country .loadedsvg g g", "click", function(e){
                clicked = 1;
                var cX = e.pageX;
                var cY = e.pageY;
                var mapId = $(this).attr("id");
                $(this).children().eq(0).css("fill","#0046AD");
                $(this).children().eq(1).css("fill","#0046AD");
                console.log(mapId, cX, cY);
                openModal(mapId, cX, cY)
            }
        )
    },

    popoverClean = function (div) {
        $(div).html("");
        $(div).css("left","-1000px");
        $(div).css("top","-1000px");
    }

    backmap = function() {
        $("body").delegate(".backmap", "click", function(){
            if ( $(this).hasClass("backToWorld") ) {
                $(".map-layer--continent").toggleClass("hidden");
                $(".map-layer--world").toggleClass("hidden");
                $(".map-layer--world").addClass("fadeIn animated");
                $(".backmap.backToWorld").toggleClass("hide");
                $(".viewallmap").toggleClass("hide");
                $('.map-layer--continent .loadedsvg').empty();
                $('.map-layer--continent').attr("c-name","");
                loadWorld();
            }
            else {
                $(".map-layer--country").toggleClass("hidden");
                $(".map-layer--world").toggleClass("hidden");
                //$(".map-layer--continent").toggleClass("hidden");
                //$(".map-layer--continent").addClass("fadeIn animated");
                //$(".backmap.backToWorld").toggleClass("hide");
                $(".backmap.backToContinent").toggleClass("hide");
                $('.map-layer--country .loadedsvg').empty();
                var id = $(".backToContinent").attr("id");
                popoverClean(".map-layer--continent .popover");
                popoverClean(".map-layer--country .popover");
                closeModal();
                goToContinent(id);
            }
        })
    },

    closeModal = function () {
        //console.log("chiudi modale");
        $(".modale").html('');
        $(".modal-close").remove();
        $(".map-layer--country .open-detail").addClass("active");
        $(".map-layer--country .modale").css("left","-1000px");
        $(".map-layer--country .modale").css("top","-1000px");
        $(".map-layer--country .modale").removeClass("toobottom");
        $(".map-layer--country .modale").removeClass("tooright");
        $(".map-layer--country .loadedsvg g ellipse").css("fill","white");
        clicked = 0;
    }

    closeModalAction = function () {
        $("body").delegate(".modal-close", "click", function(){
            closeModal();
        })
        $("body").delegate(".clicklayer", "click", function(){
            closeModal();
        })
    },

    openPopoverCity = function (mid, cX, cY) {
        console.log(mid, cX, cY);
        var positionLeft = cX;
        var positionTop = cY;
        var parentOffset = $(".map-layer--country").offset();
        var relX = positionLeft - parentOffset.left + 10;
        var relY = positionTop - parentOffset.top + 10;
        $(".map-layer--country .popover").css("left",relX+"px");
        $(".map-layer--country .popover").css("top",relY+"px");
        //popoverClean(".map-layer--country .popover");
        //console.log(mid);
        if (mid == "Sao_paolo") {
            mid = "San Paolo";
        }
        else if(mid == "hongkong") {
            mid = "Hong Kong";
        }
        else if (mid == "sanfrancisco") {
            mid = "San Francisco";
        }
        else if (mid == "los_angeles") {
            mid = "Los Angeles";
        }
        else if (mid == "new_york-2") {
            mid = "New York";
        }
        else if (mid == "singapore-2") {
            mid = "Singapore";
        }
        else if (mid == "monaco_di_baviera") {
            mid = "Monaco di Baviera";
        }
        else if (mid == "pescara") {
            mid = "Pescara-Chieti";
        }
        else if (mid == "lecce") {
            mid = "Lecce-Brindisi";
        }
        else if (mid == "lecco") {
            mid = "Lecco-Sondrio";
        }
        else if (mid == "sao_paolo") {
            mid = "San Paolo";
        }
        else if (mid == "principato_di_monaco") {
            mid = "Principato di Monaco";
        }
        else if (mid == "lussemburgo-2") {
            mid = "Lussemburgo";
        }
        else if (mid == "sidney") {
            mid = "Sydney";
        }
        else if (mid == "copenaghen") {
            mid = "Copenhagen";
        }
        else if (mid == "tokio") {
            mid = "Tokyo";
        }
        else if (mid == "shangai") {
            mid = "Shanghai";
        }

        //console.log(mid);
        $(".map-layer--country .popover").html(mid);
    },

    openPopoverCountry = function (mid, cX, cY) {
        var positionLeft = cX;
        var positionTop = cY;
        var parentOffset = $(".map-layer--continent").offset();
        var relX = positionLeft - parentOffset.left + 10;
        var relY = positionTop - parentOffset.top + 10;
        $(".map-layer--continent .popover").css("left",relX+"px");
        $(".map-layer--continent .popover").css("top",relY+"px");
        if(mid == "principatomonaco") {
            mid = "Principato di Monaco";
        }
        else if(mid == "hongkong") {
            mid = "Hong Kong";
        }
        else if(mid == "newyork") {
            mid = "New York";
        }
        else if(mid == "australia-2") {
            mid = "Australia";
        }
        else if(mid == "penisola_arabica") {
            mid = "Penisola Arabica";
        }
        $(".map-layer--continent .popover").html(mid);
    },

    popoverCity = function () {

        $("body").delegate(".map-layer--country .loadedsvg svg g g", 'mouseenter', function(e){
            //console.log("hover enter no ita");
            var cX = e.pageX;
            var cY = e.pageY;
            var mapId = $(this).attr("id");
            $(this).children().eq(0).css("fill","#0046AD");
            $(this).children().eq(1).css("fill","#0046AD");
            $(this).find("text").css("fill","#ffffff");
            openPopoverCity(mapId, cX, cY);
        })
        /*
        $("body").delegate(".map-layer--country .loadedsvg g:not('#map, #map-3, #citta')", 'mouseleave', function(e){
            //console.log("hover exit no ita");
            $(".map-layer--country .popover").html("");
            $(".map-layer--country .popover").css("left","-1000px");
            $(".map-layer--country .popover").css("top","-1000px");
        })

        $("body").delegate(".map-layer--country .loadedsvg svg g g", 'mouseenter', function(e){
            //console.log("hover enter ita");
            var cX = e.pageX;
            var cY = e.pageY;
            var mapId = $(this).attr("id");
            console.log("hover " + mapId);
            openPopoverCity(mapId, cX, cY);
        })
        */
        $("body").delegate(".map-layer--country .loadedsvg svg g g", 'mouseleave', function(e){
            //console.log("hover exit ita");
            $(".map-layer--country .popover").html("");
            $(".map-layer--country .popover").css("left","-1000px");
            $(".map-layer--country .popover").css("top","-1000px");
            if ( clicked === 0 ) {
                $(this).children().eq(0).css("fill","#ffffff");
                $(this).children().eq(1).css("fill","#ffffff");
                $(this).find("text").css("fill","#0046AD");
            } else {

            }

        })

    },

    popoverCountry = function () {
        $("body").delegate(".map-layer--continent .gotocountry", "mouseenter", function(e){
            var cX = e.pageX;
            var cY = e.pageY;
            var mapId = $(this).attr("id");
            if(is_xs()) {

            }
            else {
                $(this).children().eq(0).css("fill","#0046AD");
                $(this).children().eq(1).css("fill","#0046AD");
                $(this).find("text").css("fill","#ffffff");
                openPopoverCountry(mapId, cX, cY);
            }
        })
        $("body").delegate(".map-layer--continent .gotocountry", "mouseleave", function(e){
            $(".map-layer--continent .popover").html("");
            $(".map-layer--continent .popover").css("left","-1000px");
            $(".map-layer--continent .popover").css("top","-1000px");
            $(this).children().eq(0).css("fill","#ffffff");
            $(this).children().eq(1).css("fill","#ffffff");
            $(this).find("text").css("fill","#0046AD");
        })
    }

    closeModalGlobal = function () {
        $("body").delegate(".map-wrap", "click", function(){
            closeModal();
        });
    }

    hoverWorld = function() {
        $("body").delegate(".map-layer .gotocontinent", "mouseenter", function(e){
            if(is_xs()) {

            }
            else {
                $(this).children().eq(0).css("fill","#0046AD");
                $(this).children().eq(1).css("fill","#0046AD");
                $(this).find("text").css("fill","#ffffff");
            }
        })
        $("body").delegate(".map-layer .gotocontinent", "mouseleave", function(e){
            $(this).children().eq(0).css("fill","#ffffff");
            $(this).children().eq(1).css("fill","#ffffff");
            $(this).find("text").css("fill","#0046AD");
        })
    }

    setPositionNav = function() {
        var posTopic = $(".topicsList .navigation").offset();
        var posXnav = posTopic.left;
        $(".map .navigation").css("left",posXnav);
    }

    $document
        .ready(function() {
            //readJson();
            setPositionNav();
            loadWorld();
            //goToContinent();
            goToCountry();
            clickContinent();
            opendetail();
            closeModalAction();
            backmap();
            opendetailFromMap();
            if(is_xs()) {
                //closeModalGlobal();
            } else {
                popoverCity();
                popoverCountry();
                hoverWorld();
            }
        });

    $window
        .resize(
            function() {
                setPositionNav();
            }
        );

})(jQuery, window, document);