(function($, win, doc, undefined){

  var $html = $('html'),
      $body = $('body'),
      $window = $(window),
      $document  = $(document),

      jasonurl = "data/chapters.json";
  europeChaptersNumber = 0;
  americaChaptersNumber = 0;
  asiaChaptersNumber = 0;
  jsonData = "";
  chapterData = "";

  readJson = function() {
    $.getJSON(jasonurl, function (data) {
      jsonData = data;
      europeChaptersNumber = data.europeChapters;
      americaChaptersNumber = data.americaChapters;
      asiaChaptersNumber = data.asiaChapters;
      appendCirclesWorld();
    });
  }


  appendCirclesWorld = function () {
    //$(".map-layer.map-layer--world").append("<a continent-name='europe' data-map='1' href='javascript:;' class='circle open-continent' style='left: "+ europeX +"%; top: "+ europeY +"%;'><label>"+ europeChaptersNumber +"</label></a>")
    //$(".map-layer.map-layer--world").append("<a continent-name='america' data-map='1' href='javascript:;' class='circle open-continent' style='left: "+ americaX +"%; top: "+ americaY +"%;'><label>"+ americaChaptersNumber +"</label></a>")
    //$(".map-layer.map-layer--world").append("<a continent-name='asia' data-map='1' href='javascript:;' class='circle open-continent' style='left: "+ asiaX +"%; top: "+ asiaY +"%;'><label>"+ asiaChaptersNumber +"</label></a>")
  }

  appendCirclesContinent = function () {
    var cDim = 0;
    var indexC = 0;
    $.each(jsonData.europe, function (item, value) {
      //console.log(value);
      $.each(value, function (i, country) {
        //console.log(country[0].cities)
        //console.log(Object.keys(country[0].cities).length);
        var chaptersPerCountry = Object.keys(country[0].cities).length;
        $(".map-layer.map-layer--continent").append("<a country-name='"+country[0].name+"' data-map='1' href='javascript:;' class='circle open-country' style='left: "+country[0].posX+"%; top: "+country[0].posY+"%;'><label>"+ chaptersPerCountry +"</label></a>")
      });
    })
  }

  appendCirclesCountry = function () {
    var cDim = 0;
    var indexC = 0;
    $.each(jsonData.europe, function (item, value) {
      $.each(value, function (i, country) {
        $.each(country, function (i, city) {
          if (city.name == "italia") {
            //console.log(city.cities);
            chapterData = city.cities;
            $.each(city.cities, function (i, chapter) {
              $(".map-layer.map-layer--country").append("<a city-name='"+chapter.name+"' data-map='"+chapter.id+"' href='javascript:;' class='circle open-detail active' style='left: "+chapter.posx+"%; top: "+chapter.posy+"%;'><label>1</label><div class='modale'></div></a>")
            })
          }
        });
      });
    })
  }

  goToContinent = function () {
    $("body").delegate(".map-layer--world .open-continent", "click", function(){
          var mapName = $(this).attr("continent-name");
          //var mapId = $(this).attr("data-map");
          if ( mapName == "europe" ) {
            var imageUrl = "img/chapters/continents/"+ mapName +".svg";
            $(".map-layer--continent").css('background-image', 'url(' + imageUrl + ')');
            $(".map-layer--world").toggleClass("hidden");
            $(".map-layer--continent").toggleClass("hidden");
            $(".map-layer--continent").addClass("fadeIn animated");
            $(".viewallmap").addClass("hide");
            $(".backmap.backToWorld").toggleClass("hide");
            //appendCirclesContinent();
          }
        }
    )
  },

      goToCountry = function () {
        $("body").delegate(".map-layer--continent .open-country", "click", function(){
              //console.log("a");
              var mapName = $(this).attr("country-name");
              //var mapId = $(this).attr("data-map");
              if ( mapName == "italia" ) {
                var imageUrl = "img/countries/"+ mapName +".svg";
                //$(".map-layer--country").css('background-image', 'url(' + imageUrl + ')');
                //$(".map-layer--country .loadedsvg").append('<object data="'+ imageUrl +'">')
                $('.map-layer--country .loadedsvg').load(imageUrl);
                $(".map-layer--continent").toggleClass("hidden");
                $(".map-layer--country").toggleClass("hidden");
                $(".map-layer--country").addClass("fadeIn animated");
                $(".backmap.backToWorld").toggleClass("hide");
                $(".backmap.backToContinent").toggleClass("hide");
                appendCirclesCountry();
              }
            }
        )
      };

  opendetail = function () {
    $("body").delegate(".map-layer--country .open-detail.active", "click", function(){
          console.log("cliccato");
          var mapId = $(this).attr("data-map");

          var imgUrl = 'img/bologna.png';
          mapId = mapId - 1;
          console.log(chapterData[mapId]);
          var htmlModal = '<div class="map-modal card-list"><div class="card card--event"><div class="image-wrap" style="background-image: url('+ imgUrl +')"></div><div class="event-content-wrap"><div class="green smalltitle text-uppercase mb-1">chapter '+ chapterData[mapId].name +'</div><div class="gray midtext pb-05">Chapter Leader:<b>'+ chapterData[mapId].leader +'</b></div><div class="gray midtext mb-2">Email:<b>'+ chapterData[mapId].email +'</b></div><div class="gray midtext">'+ chapterData[mapId].text +'</div></div></div></div>';
          var modalClose = '<a class="modal-close" href="javascript:;"><span class="icon icon-close"></span></a>'
          $(this).find(".modale").append(htmlModal);
          $(this).append(modalClose);
          $(this).toggleClass("active");
        }
    )
  },

      backmap = function() {
        $("body").delegate(".backmap", "click", function(){
          console.log("b");
          if ( $(this).hasClass("backToWorld") ) {
            $(".map-layer--continent").toggleClass("hidden");
            $(".map-layer--world").toggleClass("hidden");
            $(".map-layer--world").addClass("fadeIn animated");
            $(".backmap.backToWorld").toggleClass("hide");
            $(".viewallmap").toggleClass("hide");
          }
          else {
            $(".map-layer--country").toggleClass("hidden");
            $(".map-layer--continent").toggleClass("hidden");
            $(".map-layer--continent").addClass("fadeIn animated");
            $(".backmap.backToWorld").toggleClass("hide");
            $(".backmap.backToContinent").toggleClass("hide");
          }
        })
      },

      closeModal = function () {
        $("body").delegate(".modal-close", "click", function(){
          $(".modale").html('');
          $(".modal-close").remove();
          $(".map-layer--country .open-detail").addClass("active");
        })
      },

      $document
          .ready(function() {
            readJson();
            goToContinent();
            goToCountry();
            opendetail();
            closeModal();
            backmap();
          });

})(jQuery, window, document);