(function($, win, doc, undefined){

	/*------------------------------------*\
			#utility
	\*------------------------------------*/

	var $html = $('html'),
			$body = $('body'),
			$window = $(window),
			$document  = $(document),
			$hoverElement = true,
			isIE11 = !!navigator.userAgent.match(/Trident.*rv\:11\./),
			is_safari = navigator.userAgent.indexOf("Safari") > -1,
			is_chrome   = navigator.userAgent.indexOf('Chrome') > -1;
			is_xs = function(){ return $window.width() < 765; },

			throttle = function(fn, threshhold, scope) {
				threshhold || (threshhold = 250);
				var last,
					deferTimer;
				return function () {
					var context = scope || this;

					var now = +new Date,
						args = arguments;
					if (last && now < last + threshhold) {
						// hold on to it
						clearTimeout(deferTimer);
						deferTimer = setTimeout(function () {
							last = now;
							fn.apply(context, args);
						}, threshhold);
					} else {
						last = now;
						fn.apply(context, args);
					}
				};
			},

			debounce = function(func, wait, immediate) {
				var timeout;
				return function() {
					var context = this, args = arguments;
					var later = function() {
						timeout = null;
						if (!immediate) func.apply(context, args);
					};
					var callNow = immediate && !timeout;
					clearTimeout(timeout);
					timeout = setTimeout(later, wait);
					if (callNow) func.apply(context, args);
				};
			},

			windowResize = function(){
				setHeaderTop();
			},

			windowScroll = function(){

				var scrollPos = $window.scrollTop(),
				scrollOffset = function(){
					if ($('*[data-trigger="sticky-header"]:visible').length) {
						return $('*[data-trigger="sticky-header"]:visible').first().offset().top;
					}
					// va sommata anche la outerHeight di eventuale ADV prima della
					// testata o della parte alta della skin
					return $('[data-main-header]').outerHeight();
				};

				if(is_xs()) {

					if(scrollPos > 1) {
						$('body').addClass('is-scrolled-xs');
					} else {
						$('body').removeClass('is-scrolled-xs');
					}

				} else {
					if(scrollPos > 100) {
						$('body').addClass('is-scrolled');
					} else {
						$('body').removeClass('is-scrolled');
					}
				}

				var elToCheck = $(".ifIsVisible")
				elToCheck.each(function( index ) {
					var visible = $(this).visible(true, true);
					if ( visible == true ) {
						$(this).addClass("fadeIn animated");
					}
					else {
						//elToCheck.removeClass("fadeIn animated");
					}
				});

			},

			initEventsSlick = function(){
				$('#eventSlickCarousel').slick({
					dots: false,
					infinite: false,
					speed: 300,
					slidesToShow: 4,
					slidesToScroll: 4,
					prevArrow: $('.card-prev'),
					nextArrow: $('.card-next'),
					responsive: [
						{
							breakpoint: 1024,
							settings: {
								slidesToShow: 3,
								slidesToScroll: 3,
								infinite: false,
								dots: false
							}
						},
						{
							breakpoint: 767,
							settings: {
								slidesToShow: 3,
								slidesToScroll: 2,
								infinite: false
							}
						},
						{
							breakpoint: 600,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 2,
								infinite: false
							}
						},
						{
							breakpoint: 420,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1,
								infinite: false
							}
						}
					]
				});
			},

			initNewsSlick = function(){
				$('#newsSlickCarousel').slick({
					infinite: false,
					speed: 300,
					slidesToShow: 1,
					slidesToScroll: 1,
					adaptiveHeight: true,
					prevArrow: $('.news-prev'),
					nextArrow: $('.news-next'),
					dots: false
				});
			},

			initPageEventsSlick = function(){
				$('.paginate-slick').slick({
					infinite: false,
					speed: 300,
					slidesToShow: 1,
					slidesToScroll: 1,
					adaptiveHeight: true,
					prevArrow: $('.news-prev'),
					nextArrow: $('.news-next'),
					dots: true,
					appendDots: $(".slick-pages"),
					customPaging: function(slick,index) {
						return '<a>' + (index + 1) + '</a>';
					}
				});

			},

			initNewsSlickDx = function(){
				$('.news-slick-dx').slick({
					dots: false,
					infinite: false,
					speed: 300,
					slidesToShow: 1,
					slidesToScroll: 1,
					adaptiveHeight: true,
					prevArrow: $('.news-prev-dx'),
					nextArrow: $('.news-next-dx'),
				});
			},

			initNewsSlickSx = function(){
				$('.news-slick-sx').slick({
					dots: false,
					infinite: false,
					speed: 300,
					slidesToShow: 1,
					slidesToScroll: 1,
					adaptiveHeight: true,
					prevArrow: $('.news-prev-sx'),
					nextArrow: $('.news-next-sx'),
				});
			},

			initScrollMagic = function() {
				// init



				var ctrl = new ScrollMagic.Controller({
					globalSceneOptions: {
						triggerHook: 'onLeave'
					}
				});

				var scene = new ScrollMagic.Scene({
					triggerElement: '.top-entry',
					offset: 250,
					duration: 350
				})
					//.setClassToggle('.top-entry', 'animate-close')
					//.setClassToggle('.top-entry .square-mask','zoomOut')
					//.setClassToggle('.top-entry .square-mask','animated')

					.on('enter',function(event){
						//var networkSquare = $('.top-network .big-square');
						var square = $('.top-entry .square-mask');
						var toptext	= $(".text-wrap");
						//console.log(event.scrollDirection);
						if (event.scrollDirection === "REVERSE") {
							square.removeClass('zoomOut');
							square.addClass('zoomIn');

							//networkSquare.removeClass('zoomOut');
							//networkSquare.addClass('zoomIn');

							toptext.removeClass('slideOutLeft');
							toptext.addClass('slideInLeft');
						}
						if (event.scrollDirection === "FORWARD") {
							square.removeClass('zoomIn');
							square.addClass('zoomOut');

							//networkSquare.removeClass('zoomIn');
							//networkSquare.addClass('zoomOut');

							toptext.removeClass('slideInLeft');
							toptext.addClass('slideOutLeft');
						}
					})
					.on('leave',function(event){
						var square = $('.top-entry .square-mask');
						var toptext	= $(".text-wrap");
						//console.log(event.scrollDirection);
						if (event.scrollDirection === "REVERSE") {
							square.removeClass('zoomOut');
							square.addClass('zoomIn');

							//networkSquare.removeClass('zoomOut');
							//networkSquare.addClass('zoomIn');

							toptext.removeClass('slideOutLeft');
							toptext.addClass('slideInLeft');
						}
						if (event.scrollDirection === "FORWARD") {
							square.removeClass('zoomIn');
							square.addClass('zoomOut');

							//networkSquare.removeClass('zoomIn');
							//networkSquare.addClass('zoomOut');

							toptext.removeClass('slideInLeft');
							toptext.addClass('slideOutLeft');
						}
					})

					.reverse(true)
					//.addIndicators()
					.addTo(ctrl);


			},

			initEventsHp = function() {
				var eventCard = $('section .card');
				var time = 500;
				eventCard.each(function(i){
					var card = $(this);
					setTimeout(function() {
						card.toggleClass('fadeIn');
						card.toggleClass('animated');
					}, 200*i);
				});
			},

			initNewsHp = function() {

			},

			initScrollStickyHeader = function() {
				if ($window.scrollTop() >= 100) {
					console.log("a");
					$("body").addClass("fixed-header");
				} else {
					console.log("b");
					$("body").removeClass('fixed-header');
				}
			},

		initEventZoom = function() {
			var delay=500, setTimeoutConst;
			var htmlToMove = "";
			var thisEl = "";

			var groupwrap = $(".card--event");
			groupwrap.each(function( index ) {

			})



			$("body").delegate(".card--event", "mouseenter", function(e){
					var hElement = $(this).outerHeight();
					$(this).css("height", hElement);
					$(".card--event").removeClass("hover fadeIn animated");
					$(this).addClass("hover fadeIn animated");
				}
			)
			$("body").delegate(".card--event", "mouseleave", function(e){
					$(this).css("height", "auto");
					$(this).removeClass("hover fadeIn animated");
				}
			)
		},

		topicsHover = function() {
			$(".topicsList .el").mouseenter(
				function () {
					$(".topicsList .el").removeClass("hover");
					$(this).toggleClass("hover");
					//$(this).find(".el-hover").addClass("fadeIn animated");
				}
			)
			$(".topicsList .el").mouseleave(
				function () {
					$(this).toggleClass("hover");
					//$(this).find(".el-hover").removeClass("fadeIn animated");
				}
			)
		},

			showPanelSm = function() {
				$(".showZoom").click(
					function () {
						var thisEl = $(this).parent(".card");
						thisEl.find(".absbr").addClass("hide");
						$(thisEl).find('.zoom--panel').addClass('in animated');
						thisEl.find(".image-wrap").clone().appendTo( $(thisEl).find('.zoom--panel') );
						thisEl.find(".event-content-wrap").clone().appendTo( $(thisEl).find('.zoom--panel') );
						thisEl.find(".close-layer").removeClass("hide");
					}
				)
			},

			closePanelSm = function () {
				$(".close-layer").click(
					function () {
						// console.log("a");

						var thisEl = $(this).parents(".card");
						thisEl.find(".showZoom").removeClass("hide");
						thisEl.find('.zoom--panel').empty();
						thisEl.find(".close-layer").addClass("hide");
					}
				)
			},

			dateRange = function() {
				var groupwrap = $(".formDateGroup");
				groupwrap.each(function( index ) {
					var inputA = $(this).find(".formDateA");
					var inputB = $(this).find(".formDateB");
					inputA.datetimepicker({
						format: 'DD/MM/YYYY'
					});
					inputB.datetimepicker({
						format: 'DD/MM/YYYY',
						useCurrent: false
					});
					inputA.on("dp.change", function (e) {
						inputB.data("DateTimePicker").minDate(e.date);
					});
					inputB.on("dp.change", function (e) {
						inputB.data("DateTimePicker").maxDate(e.date);
					});
				});
			},

			isMobileDevice = function()  {
				if(is_xs()) {
					// is mobile
					$("body").addClass("isMobile");
				}
				else {
					// is desktop
				}
			};

			setHeaderTop = function() {
				if(is_xs()) {
					if ( $( "#disservizioBox" ).length ) {
						var hDisservizio = $( "#disservizioBox" ).outerHeight();
						$("header.main-header").css("top",hDisservizio);
					}
				}
			}


			carrerZindex = function() {
				$("body").delegate(".blue-career-wrap .contents_wrapper .card-block.card-block-link", "mouseenter", function(e){
						$(".blue-career-wrap").addClass("z-xl");
					}
				)
				$("body").delegate(".blue-career-wrap .contents_wrapper .card-block.card-block-link", "mouseleave", function(e){
						$(".blue-career-wrap").removeClass("z-xl");
					}
				)
			}

			threedotskMobilePopover = function() {
				$(".dropdown-toggle").click(
					function () {
						if (is_xs()) {
							var posxBtn = $('.btn-action-popover').offset().left;
							//console.log(posxBtn);
							if (posxBtn > 50) {
								$('.action-btn .dropdown-menu').addClass('toRight');
							}
							else {
								$('.action-btn .dropdown-menu').removeClass('toRight');
							}
						}
					}
				)
			}

			documentReadyFn = function() {
				setTimeout(function() {
					initScrollMagic();
					if( is_safari || isIE11 ) {
						if ( is_chrome ) {
							$(".text-wrap").addClass('slideInLeft');
						}
					} else {
						console.log("altro browser");
						$(".text-wrap").addClass('slideInLeft');
					}
				}, 5);
				setTimeout(function() {
					initNewsSlick();
					initEventsSlick();
					initNewsSlickDx(),
					initNewsSlickSx();
					showPanelSm();
					closePanelSm();
					initPageEventsSlick();
					isMobileDevice();
					setHeaderTop();
				}, 15);
				setTimeout(function() {
					initEventZoom();

					if ( $(".topicsList .el").length ) {
						$(".topicsList .el").removeClass("hover");
					}

				}, 1500);
				topicsHover();
				dateRange();
				threedotskMobilePopover();
				//carrerZindex();
				//initNewsHp();
			};

	/*------------------------------------*\
			doc ready
	\*------------------------------------*/

	$document
		.ready(function() {
			documentReadyFn();
		});


	/*------------------------------------*\
			window
	\*------------------------------------*/
		$window
			.resize(debounce(windowResize))
			.scroll(throttle(windowScroll))
			//.resize(debounce(stickyRecalc))
})(jQuery, window, document);
