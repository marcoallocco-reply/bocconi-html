// including plugins
var gulp = require('gulp');
var clean = require('gulp-clean');
var util = require( 'gulp-util' );
var concat = require( 'gulp-concat' );
var pug = require('gulp-pug');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var cssmin = require('gulp-cssmin');
var uglify = require( 'gulp-uglify' );
var gulpsync = require('gulp-sync')(gulp);
//var bs = require('browser-sync').create('Dev Server');


//  CLEAN  //
gulp.task('clean', function () {
  return gulp.src( ['./_templates/build'] )
      .pipe(clean());
});

//  PUG  //
gulp.task('templates', function() {
  return gulp.src( ['./_templates/sources/pug/*.pug' ])
      .pipe(pug({
        pretty: true
      }))
      .pipe(gulp.dest('./_templates/build'));
});

//  SASS  //
gulp.task('sass', function () {
  return gulp.src(['./_templates/sources/scss/style.scss', './_templates/sources/scss/sole24sans.scss'])
      .pipe(sass().on('error', sass.logError))
      .pipe(sourcemaps.write('/'))
      .pipe(gulp.dest('./_templates/build/css'))
      .pipe(cssmin())
      .pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest('./_templates/build/css'));
});

gulp.task('css-dev', () => {
  return gulp.src('./_templates/sources/scss/style.scss')
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('./_templates/build/css'));
});

//  COPY IMAGES  //
gulp.task( 'images', function() {
  return gulp.src('./_templates/sources/img/**/*.*')
      .pipe( gulp.dest( './_templates/build/img' ) );
});

//  COPY SCRIPTS  //
gulp.task( 'scripts', function() {
  return gulp.src('./_templates/sources/js/map-script.js')
      .pipe( gulp.dest( './_templates/build/js' ) );
});

//  COPY FONTS  //
gulp.task( 'fonts', function() {
  return gulp.src('./_templates/sources/fonts/**/*.*')
      .pipe(gulp.dest('./_templates/build/fonts'));
});

//  COPY DATA  //
gulp.task( 'data', function() {
  return gulp.src('./_templates/sources/data/**/*.*')
      .pipe(gulp.dest('./_templates/build/data'));
});

//  JS  //
var jsFiles = [

  './node_modules/jquery-visible/jquery.visible.js',
  './node_modules/slick-carousel/slick/slick.js',
  './node_modules/scrollmagic/scrollmagic/minified/ScrollMagic.min.js',
  './node_modules/scrollmagic/scrollmagic/minified/plugins/debug.addIndicators.min.js',

  '_templates/sources/js/app.js',
];
gulp.task('js-min', () => {
  return gulp.src(jsFiles)
      .pipe(concat('app.js'))
      .pipe(uglify())
      .pipe(gulp.dest('./_templates/build/js'));
});

gulp.task('default', gulpsync.sync([
  'clean',
  'sass',
  'css-dev',
  'templates',
  "images",
  "fonts",
  "scripts",
  "data",
  "js-min"
]));

gulp.task('dev', gulpsync.sync([
  'clean',
  'sass',
  'templates',
  "images",
  "fonts",
  "js-min"
]));
